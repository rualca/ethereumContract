pragma solidity ^0.4.19;

contract TokenERC20 {
    string public name;
    string public symbol;
    uint8 public decimals = 18;
    uint256 public totalSupply;
    mapping (address => uint256) public balanceOf;
    event Transfer(address from, address to, uint256 value);
    
    function TokenERC20(uint256 initialSupply, string tokenName, string tokenSymbol) public {
        totalSupply = initialSupply * 10 ** uint256(decimals);
        name = tokenName;
        symbol = tokenSymbol;
        balanceOf[msg.sender] = totalSupply;
    }
    
    function _transfer(address _from, address _to, uint256 _value) internal {
        require(balanceOf[_from] >= _value);
        require(balanceOf[_to] + _value > _value);
        uint256 previousBalances = balanceOf[_from] + balanceOf[_to];
        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        Transfer(_from, _to, _value);
        assert(balanceOf[_from] + balanceOf[_to] == previousBalances);
    }
    
    function transfer(address to, uint256 value) public {
        _transfer(msg.sender, to, value);
    }
}

interface TokenERC20Interface {
    function transfer(address to, uint256 value) public;
}

contract BasicContract {
    function transferToken() public {
        TokenERC20Interface token = TokenERC20Interface(0xbbf289d846208c16edc8474705c748aff07732db);
        token.transfer(0x583031d1113ad414f02576bd6afabfb302140225, 1);
    }
}